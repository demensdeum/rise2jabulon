import sys
import os
import json
import shutil
from distutils.dir_util import copy_tree

name = "Rise 2 Jabulon"
version = "1.0.0"

command = sys.argv[1]

if command == "--version":
    print(f"{name} - {version}")
    exit(0)

projectDirectoryPath = command

currentPath = os.getcwd()
print(f"Current path: {currentPath}")

outputDirectory = sys.argv[2]
outputDirectory = f"{currentPath}/{outputDirectory}"

os.chdir(projectDirectoryPath)

projectFile = open("rise2-project.json")
projectFileContent = projectFile.read()
projectFile.close()
projectFileJson = json.loads(projectFileContent)
print(projectFileJson)

projectName = projectFileJson["name"]

print(f"Building \"{projectName}\"")

rise2binaryPath = os.environ['RISE2']

print(rise2binaryPath)

currentPath = os.getcwd()

print(f"Output directory: {outputDirectory}")

try:
    shutil.rmtree(outputDirectory)
except OSError as error:
    print(error)

os.makedirs(outputDirectory)

transpiledFiles = []

sourceCodePath = f"{currentPath}/src"

for root, dirs, files in os.walk(sourceCodePath):
    for file in files:
        print(f"file: {file}")
        if file.endswith(".ri2"):
            filename = file.split(".")[0]
            filepath = f"{sourceCodePath}/{file}"
            shutil.copyfile(filepath, f"{outputDirectory}/{file}")
            if filename == "main":
                command = f"python3 {rise2binaryPath} {outputDirectory}/{file} {outputDirectory}/main.cpp \"<NONE>\""
            else:
                command = f"python3 {rise2binaryPath} {outputDirectory}/{file} {outputDirectory}/{filename}.hpp \"<NONE>\""
                
            print(command)
            os.system(command)
            if filename == "main":
                transpiledFiles.append(f"{filename}.cpp")
            else:
                transpiledFiles.append(f"{filename}.hpp")

shutil.copyfile("rise2-project.json", f"{outputDirectory}/rise2-project.json")

try:
    resourcesPath = f"{currentPath}/resources"
    copy_tree(resourcesPath, outputDirectory)
except BaseException as exception:
    print(exception)

print(transpiledFiles)

os.chdir(outputDirectory)
rise2beckettBinaryPath = os.environ['RISE2BECKETT']
print(rise2beckettBinaryPath)
command = f"python3 {rise2beckettBinaryPath} ."
os.system(command)

rise2jabulonResourcesPath = os.environ['RISE2JABULONRESOURCES']
print(rise2jabulonResourcesPath)

shutil.copyfile(f"{rise2jabulonResourcesPath}/CMakeLists-Template.txt", f"{outputDirectory}/CMakeLists.txt")
cmakeTemplateFile = open(f"{outputDirectory}/CMakeLists.txt", "r")
cmakeTemplateFileContent = cmakeTemplateFile.read()
cmakeTemplateFile.close()

rise2executableName = projectFileJson["executable"]
try:
    dependencies = projectFileJson["dependencies"]
except:
    dependencies = []
cmakeTemplateFileContent = cmakeTemplateFileContent.replace("RISE2PROJECTNAME", rise2executableName)
cmakeTemplateFileContent = cmakeTemplateFileContent.replace("RISE2EXECUTABLENAME", rise2executableName)
cmakeTemplateFileContent = cmakeTemplateFileContent.replace("RISE2TRANSPILEDSOURCECODEFILES", " ".join(transpiledFiles))
cmakeTemplateFileContent = cmakeTemplateFileContent.replace("RISE2TRANSPILEDSOURCECODEDEPENDENCIES", " ".join(dependencies))

cmakeTemplateFile = open(f"{outputDirectory}/CMakeLists.txt", "w")
cmakeTemplateFile.write(cmakeTemplateFileContent)
cmakeTemplateFile.close()

os.system("cmake .")
os.system("make")

os.system(f"./{rise2executableName}")